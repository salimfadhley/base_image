FROM python:latest

ENTRYPOINT ["python"]

ENV OPENCV_VERSION="3.4.2"
# Should correspond to a tag in the PyMesh project
ENV PYMESH_VERSION="v0.2.1"
ENV PYMESH_PATH /src/pymesh

RUN apt-get update && \
        apt-get install -y \
        apt-utils \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavformat-dev \
        libpq-dev \
        libgmp-dev \
        libmpfr-dev \
        libgmpxx4ldbl \
        libboost-dev \
        libboost-thread-dev \
        && atpt-get clean

# PYMESH
WORKDIR $PYMESH_PATH
RUN git checkout tags/$PYMESH_VERSION

RUN git submodule update --init --recursive && \
rm -rf $PYMESH_PATH/third_party/build && \
rm -rf $PYMESH_PATH/build && \
mkdir -p $PYMESH_PATH/third_party/build && \
mkdir -p $PYMESH_PATH/build && \

# Actually install PyMesh
pip install -r $PYMESH_PATH/python/requirements.txt

RUN ./setup.py bdist_wheel && rm -rf build third_party/build && pip install dist/pymesh2-0.1.14-cp36-cp36m-linux_x86_64.whl
WORKDIR /root/

# Test PyMesh
RUN python -c "import pymesh; pymesh.test()"

# Numpy
RUN pip install numpy

# OpenCV
WORKDIR /
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip \
&& unzip ${OPENCV_VERSION}.zip \
&& mkdir /opencv-${OPENCV_VERSION}/cmake_binary \
&& cd /opencv-${OPENCV_VERSION}/cmake_binary \
&& cmake -DBUILD_TIFF=ON \
  -DBUILD_opencv_java=OFF \
  -DWITH_CUDA=OFF \
  -DWITH_OPENGL=ON \
  -DWITH_OPENCL=ON \
  -DWITH_IPP=ON \
  -DWITH_TBB=ON \
  -DWITH_EIGEN=ON \
  -DWITH_V4L=ON \
  -DBUILD_TESTS=OFF \
  -DBUILD_PERF_TESTS=OFF \
  -DCMAKE_BUILD_TYPE=RELEASE \
  -DCMAKE_INSTALL_PREFIX=$(python3.7 -c "import sys; print(sys.prefix)") \
  -DPYTHON_EXECUTABLE=$(which python3.7) \
  -DPYTHON_INCLUDE_DIR=$(python3.7 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
  -DPYTHON_PACKAGES_PATH=$(python3.7 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
  .. \
&& make install \
&& rm /${OPENCV_VERSION}.zip \
&& rm -r /opencv-${OPENCV_VERSION}

